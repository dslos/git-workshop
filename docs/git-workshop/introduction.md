---
sidebar_position: 1
---

# Introduction

## Why should you version control dd?

Keeping track of changes to your files done by yourself or your collaborators. At any moment you can exploit the history of the project to see who wrote what on a particular day. It even allows you to go back to a specific version or undo specific edits.

Synchronizes files between different people or infrastructures (i.e. laptops, servers, …), making it a powerful collaborating system.

Testing new code/changes. Git can control multiple alternative versions of the same project in which you can make some changes and only when you or your collaborators are happy with hem, you can include them in the main version.

![Git Overview](image.png)

## Markdown
Markdown is a lightweight markup language that you can use to add formatting elements to plaintext text documents. Created by John Gruber in 2004, Markdown is now one of the world’s most popular markup languages.

Using Markdown is different than using a WYSIWYG editor. In an application like Microsoft Word, you click buttons to format words and phrases, and the changes are visible immediately. Markdown isn’t like that. When you create a Markdown-formatted file, you add Markdown syntax to the text to indicate which words and phrases should look different.

For example, to denote a heading, you add a number sign before it (e.g., # Heading One). Or to make a phrase bold, you add two asterisks before and after it (e.g., \*\*this text is bold**`). It may take a while to get used to seeing Markdown syntax in your text, especially if you’re accustomed to WYSIWYG applications. The screenshot below shows a Markdown file displayed in the Visual Studio Code text editor.

### Basic markdown syntax

| Element         | Markdown Syntax                  |
|-----------------|----------------------------------|
| [Heading](https://www.markdownguide.org/basic-syntax/#headings)        | # H1                             |
|                 | ## H2                            |
|                 | ### H3                           |
| [Bold](https://www.markdownguide.org/basic-syntax/#bold)            | \*\*bold text\**                    |
| [Italic](https://www.markdownguide.org/basic-syntax/#italic)          | *italicized text*                |
| [Blockquote](https://www.markdownguide.org/basic-syntax/#blockquotes-1)      | > blockquote                     |
| [Ordered List](https://www.markdownguide.org/basic-syntax/#ordered-lists)    | 1. First item <br /> 2. Second item <br /> 3. item |
| [Unordered List](https://www.markdownguide.org/basic-syntax/#unordered-lists)  | - First item <br /> - Second item <br />- Third item |
| [Code](https://www.markdownguide.org/basic-syntax/#code)            | \`code`                           |
| [Horizontal Rule](https://www.markdownguide.org/basic-syntax/#horizontal-rules) | ---                              |
| [Link](https://www.markdownguide.org/basic-syntax/#links)            | \[title]\(https://www.example.com) |
| [Image](https://www.markdownguide.org/basic-syntax/#images-1)           | \![alt text]\(image.jpg)           |
| Fenced Code Block |\```<br /> \{ <br />"firstName": "John",<br />"age": 25 <br /> }<br />``` |

## Visual Studio code

Visual Studio Code is a lightweight but powerful source code editor which runs on your desktop and is available for Windows, macOS and Linux. It comes with built-in support for JavaScript, TypeScript and Node.js and has a rich ecosystem of extensions for other languages and runtimes (such as C++, C#, Java, Python, PHP, Go, .NET).

Visual studio can preview markdown as html and hightlight the syntax while editing as illustrated below:

![Visual studio code](image-2.png)

