---
sidebar_position: 2
---

# Tutorial

## Create a new repository
1. Browse to https://gitlab.ilvo.be/
2. Create new blank project with following settings (change to your username)

![Alt text](image-1.png)

## Clone repository with SSH (command line)

Open a terminal on the server and go to a folder where you want to keep your git files
Clone the repository you made
```
git clone git@gitlab.ilvo.be:dslos/git-workshop.git
```

This will download the contents of the git repository, including the README.md
The .md extension stands for markdown, one of the popular markup languages.

## Practice Markdown yourself

Edit the markdown file with the basic syntax to document the step you did so far and add some screenshots.

:::tip

Some **content** with _Markdown_ `syntax`. Check [this example](https://markdown-it.github.io/).
:::

## Git status
Once you made changes to the readme you can view which files were edited. 


![Alt text](image-3.png)

## Git add & commit

When we made change to a file, the changes should be added and commited with git. The procedure is illustrated below:

![Alt text](image-5.png)

Now you are ready to add your changes

```
git add README.md
```

Next we can commit our changes
```
git commit -m 'add git doc' README.md
```

The -m adds a message to the commit
![Alt text](image-4.png)

## Git push

Now we want to add our change to the remote repository.

1. First let’s check which branch we are on:
```
git branch
```
![Alt text](image-6.png)

Here we are on the master branch. This is the branch where chances where commited. So this is the branch that needs to be pushed to the remote.

```
git push origin master
```

![Alt text](image-7.png)

Congratulations, you did your first commits with git and pushed them to a remote repository. Your remote branch should now be updated with the last chances. You can observe the history on gitlab to see the history of the commits:

![Alt text](image-8.png)


## Git and Markdown practice

You can do the same workflow if you edited multiple files with some extra parameters.

Make a new folder in your git folder and add two new markdown files to it


:::tip

`git add --all` adds all changed files at once

`git commit --all -m 'commit message for all files'` #This commits all files at once with the same commit message

:::

### Github Desktop app

We start from zero, let’s delete the git repository locally again but clone the remote repository again with github desktop

![Alt text](image-9.png)

The first time, you will need your personal access token to be able to clone a repository from the ILVO gitlab

You can also view the history of all commits you did locally

![Alt text](image-10.png)